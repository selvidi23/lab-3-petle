#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void horizontal_line(int n_chars){

    for(int i=0; i<n_chars; i++){
        printf("$");
    }
    printf("\n");
}

void vertical_line(int n_chars){

    for(int i=0; i<n_chars; i++){
        printf("$\n");
    }

}

void filed_rectangle(int a, int b){

    for(int i=0; i<a; i++){
        for(int j = 0; j<b; j++){
            printf("$");
        }
        printf("\n");
    }
}

void horizontal_line_b(int left_side_sp, int b){

    for(int i=0; i<left_side_sp; i++){
        printf(" ");
    }
    for(int j=0; j<b; j++){
        printf("$");
    }
    printf("\n");
}

void vertical_line_b(int left_side_sp, int b){
    for(int i=0; i<b; i++){
        for(int j=0; j<b; j++){
            printf(" ");
        }
        printf("$\n");
    }
}

void filed_rectangle_b(int left_side_sp, int a, int b){
    for(int i=0; i<a; i++){
        for(int j=0; j<left_side_sp; j++){
            printf(" ");
        }
        for(int k=0; k<b; k++){
            printf("$");
        }
        printf("\n");
    }
}

void empty_rectangle_b(int left_side_sp, int a, int b){
    for(int i=0; i<a; i++){
        for(int j=0; j<left_side_sp; j++){
            printf(" ");
        }
        for(int k=0; k<b; k++){
            if(k == 0 || k == b-1 || i == 0 || i == a -1){
                printf("$");
            }
            else{
                printf(" ");
            }
           
        }
        printf("\n");
    }
}

void diagonal_line(int a){
    
    for(int i = a; i!=1; i--){
        for(int j = i; j!=1; j--){
            printf(" ");
        }
        printf("&\n");
    }
    printf("&\n");
}

void rectangular_triangle(int a){
    
    for(int i = 0; i <= a; i++ ){
        for(int j = 0; j < i; j++){
            printf("$");
        }
        printf("\n");
    }

}




int main()
{

int select;
int a,b,break_length;

    do{
        printf("\n");
        printf("Wybierz dzialanie\n\n");

        printf("1 - Pozioma Linia\n");
        printf("2 - Pionowa Linia\n");
        printf("3 - Wypelniony Prostokat\n");
        printf("4 - Pozioma Linia Odsunieta Od Lewej Strony \n");
        printf("5 - Pionowa Linia Odsunieta Od Lewej Strony \n");
        printf("6 - Wypelniony Prostokat Odsuniety Od Lewej Strony  \n");
        printf("7 - Pusty Prostokat Odsuniety Od Lewej Strony  \n");
        printf("8 - Linia Ukosna\n");
        printf("9 - Trojkat Prostokatny\n");
        printf("0 - Koniec\n\n");

        scanf("%d",&select);

        printf("\n");

        switch(select){
        case 1:
            printf("Podaj dlugosc\n");
            scanf("%d",&a);
            printf("\n");
            horizontal_line(a);
            break;

        case 2:
            printf("Podaj dlugosc\n");
            scanf("%d",&a);
            printf("\n");
            vertical_line(a);
            break;

        case 3:
            printf("Podaj dlugosc\n");
            scanf("%d",&a);
            printf("Podaj wysokosc\n");
            scanf("%d",&b);
            printf("\n");
            filed_rectangle(b,a);
            break;

        case 4:
            printf("Podaj dlugosc przerwy od lewej strony\n");
            scanf("%d",&break_length);
            printf("Podaj dlugosc\n");
            scanf("%d",&b);
            printf("\n");
            horizontal_line_b(break_length,b);
            break;

        case 5:
            printf("Podaj dlugosc od lewej strony\n");
            scanf("%d",&break_length);
            printf("Podaj dlugosc\n");
            scanf("%d",&b);
            printf("\n");
            vertical_line_b(break_length,b);
            break;

        case 6:
            printf("Podaj dlugosc przerwy\n");
            scanf("%d",&break_length);
            printf("Podaj dlugosc\n");
            scanf("%d",&a);
            printf("Podaj wysokosc\n");
            scanf("%d",&b);
            printf("\n");
            filed_rectangle_b(break_length,b,a);
            break;

        case 7:
            printf("Podaj dlugosc przerwy\n");
            scanf("%d",&break_length);
            printf("Podaj wysokosc\n");
            scanf("%d",&a);
            printf("Podaj dlugosc\n");
            scanf("%d",&b);
            printf("\n");
            empty_rectangle_b(break_length,a,b);
            break;

        case 8:
            printf("Podaj wysokosc\n");
            scanf("%d",&a);
            printf("\n");
            diagonal_line(a);
            break;

        case 9:
            printf("Podaj długosc\n");
            scanf("%d",&a);
            printf("\n");
            rectangular_triangle(a);
            break;

        }
        
    }while(select != 0);

    return 0;
}
